package com.employee.EmployeeMaster.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.employee.EmployeeMaster.model.Employee;
import com.employee.EmployeeMaster.repository.EmployeeRepository;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1")
public class EmployeeController {
	@Autowired
    private EmployeeRepository employeeRepository;

    @GetMapping("/employees")
    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    @GetMapping("/employees/{id}")
    public ResponseEntity<Employee> getEmployeeById(@PathVariable(value = "id") Long employeeId){
        Optional<Employee> employees = employeeRepository.findById(employeeId);
        Employee employee = employees.get();
        return ResponseEntity.ok().body(employee);
    }
    
    @PostMapping("/employees")
    public Employee createEmployee(@Valid @RequestBody Employee employee) {
        return employeeRepository.save(employee);
    }

    @PutMapping("/employees/{id}")
    public ResponseEntity<Employee> updateEmployee(@PathVariable(value = "id") Long employeeId,
         @Valid @RequestBody Employee employeeDetails){
        Optional<Employee> employees = employeeRepository.findById(employeeId);
        Employee employee = employees.get();

        employee.setEmployeeId(employeeDetails.getEmployeeId());
        employee.setLastName(employeeDetails.getLastName());
        employee.setFirstName(employeeDetails.getFirstName());
        final Employee updatedEmployee = employeeRepository.save(employee);
        return ResponseEntity.ok(updatedEmployee);
    }

    @DeleteMapping("/employees/{id}")
    public Map<String, Boolean> deleteEmployee(@PathVariable(value = "id") Long employeeId){
        Optional<Employee> employees = employeeRepository.findById(employeeId);
        Employee employee = employees.get();

        employeeRepository.delete(employee);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
    @GetMapping("/employees/check/{empId}/{id}")
    public Boolean getEmployeeById(@PathVariable(value = "empId") String employeeId,
    		@PathVariable(value = "id") Long id){
        Boolean isUsed = false;
        Employee employee = null;
        if(id == 0){
        	employee = employeeRepository.findByEmployeeId(employeeId);
        	if(employee != null){
            	isUsed = true;
            }
        } else {
        	 employee = employeeRepository.findByEmployeeIdAndIdNot(employeeId,id);
        	 if(employee != null){
        		 isUsed = true;
             }
        }
       return isUsed;
    }
}
