package com.employee.EmployeeMaster.Model;

import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "employees")
public class Employee {
	private long id;
    private String firstName;
    private String lastName;
    private String employeeId;
    private Boolean gender;
    private BigInteger role;
    private Integer pincode;
    private String email;
    
    public Employee() {
    	  
    }
 
    public Employee(String firstName, String lastName, String employeeId,Boolean gender,BigInteger role,String Email,Integer pincode) {
         this.firstName = firstName;
         this.lastName = lastName;
         this.employeeId = employeeId;
         this.gender = gender;
         this.role = role;
         this.email = email;
         this.pincode = pincode;
    }
 
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
        public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
 
    @Column(name = "first_name", nullable = false)
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
 
    @Column(name = "last_name", nullable = false)
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
 
    @Column(name = "employee_id", nullable = false)
    public String getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

    @Override
    public String toString() {
        return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", employeeId=" + employeeId
        		+ ", gender=" + gender + ", role=" + role + ", email=" + email +  ", pincode=" + pincode + "]";
    }
    @Column(name = "gender")
	public Boolean getGender() {
		return gender;
	}

	public void setGender(Boolean gender) {
		this.gender = gender;
	}

	@Column(name = "role")
	public BigInteger getRole() {
		return role;
	}

	public void setRole(BigInteger role) {
		this.role = role;
	}

	@Column(name = "pin_code")
	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	@Column(name = "email")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	
}
