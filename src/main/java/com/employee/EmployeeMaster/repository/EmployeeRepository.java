package com.employee.EmployeeMaster.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.employee.EmployeeMaster.model.Employee;


public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	Employee findByEmployeeIdAndId(String employeeId,Long id);
	Employee findByEmployeeId(String employeeId);
	Employee findByEmployeeIdAndIdNot(String employeeId,Long id);
}
